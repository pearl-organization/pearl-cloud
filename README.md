# pearl-boot

#### 介绍
微服务开发框架

#### 软件架构
软件架构说明

1 ，定义名称：peral  珍珠;人造珍珠;(形状或颜色)像珍珠之物;极有价值的东西



2. 模块分层：
   pearl-cloud
    -  pearl-base (统一响应结果集，状态码、通用枚举)
    -  pearl-util （工具类、hutools、apache commons、lombok 、mapstarct）
    -  pearl-boot（自定义banner、自定义启动类SpringApplication、统一日志文件、读取默认配置（自动配置或者加载当前JAR包YML配置）、统一异常处理、）
    -  pearl-cloud (自定义启动类注解)
    -  pearl-bom （版本控制）
    -  pearl-boot-starters
        - pearl-boot-starter-security
        - pearl-mybatis-plus-boot-starter
        - pearl-boot-starter-log
        - pearl-boot-starter-sms
        - pearl-web-boot-starter
        - pearl-webflux-boot-starter
        - pearl-boot-starter-oss
        - pearl-redis-boot-starter
    -  pearl-cloud-starters
        - pearl-nacos-discovery-cloud-starter
        - pearl-nacos-config-cloud-starter
        - pearl-seata-cloud-starter
        - pearl-dubbo-cloud-starter
        - pearl-cloud-starter-sleuth
        - pearl-cloud-starter-sentinel
        - pearl-cloud-starter-security-oauth2-resouer-server
          2.0 业务工程
          开源版
    - pearl-project
        - pearl-auth
        - pearl-gataway
        - pearl-system-manager
          商业版
    - pearl-project
        - pearl-auth
        - pearl-gataway
        - pearl-system-manager

2.1微服务开发框架
Spring Boot 单体
Spring Cloud 微服务
Istio 服务网格
Gradle 7.4.2
2.2 基础服务开发平台  
后台代码生成器 （生成增删改查）
大屏拖拽	生成器
工作流
前端拖拽	生成器
后台管理（菜单、角色、用户、机构、权限）
OSS 文件，本地、Minio、阿里、腾讯 、华为、
消息队列，Rabbit Rocket
短信平台，阿里、腾讯、大鱼
支付，微信、支付宝、英联、数字货币
SSO认证中心，Oauth 登录，授权管理
API网关，限流、路由、XSS
2.3



3. 技术框架
   Spring Boot
   Spring Cloud Alibaba
   Mysql 8.0
   HUtools
   JackSon
   Redis 7
   ES 7
   nacos 2.1
   Seata 1.5



4. 功能：
#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
