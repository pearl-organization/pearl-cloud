package com.pearl.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author TangDan
 * @version 1.0
 * @since 2022/10/14
 */
@SpringBootApplication
public class PearlGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PearlGatewayApplication.class, args);
	}

}
