package com.pearl.service.upms;

import com.pearl.boot.PearlSpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PearlCloudAdminApplication {

    public static void main(String[] args) {
        PearlSpringApplication.run("后台管理", PearlCloudAdminApplication.class, args);
    }

}
