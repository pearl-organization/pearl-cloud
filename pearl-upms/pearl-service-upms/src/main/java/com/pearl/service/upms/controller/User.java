package com.pearl.service.upms.controller;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author TangDan
 * @version 1.0
 * @since 2022/10/14
 */
@Data
@Schema(title= "用户基本信息")
public class User {

    @Schema(title="姓名")
    String userName;
    @Schema(title="密码")
    String password;
}
