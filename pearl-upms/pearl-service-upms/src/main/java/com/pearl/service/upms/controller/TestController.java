package com.pearl.service.upms.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

/**
 * @author TangDan
 * @version 1.0
 * @since 2022/10/14
 */
@RequestMapping(value = "/test")
@RestController
@Tag(name = "用户管理")
public class TestController {


    @PostMapping("/create")
    @Operation(summary = "创建用户", description = "创建用户详细描述")
    public String value(@RequestBody User user) {
        return "";
    }

    @GetMapping("/create")
    @Parameters(
            @Parameter(description = "名称", example = "张三", name = "name")
    )
    @Operation(summary = "查询用户", description = "创建用户详细描述")
    public String value(@RequestParam String name) {
        return "";
    }


}
