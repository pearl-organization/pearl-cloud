package com.pearl.cloud.common;

/**
 * @author TangDan
 * @version 1.0
 * @since 2022/10/14
 */
public interface ApplicationVersion {

    String AUTH_SERVER = "认证服务";
}
