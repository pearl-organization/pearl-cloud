package com.pearl.cloud.auth;

import com.pearl.boot.PearlSpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PearlCloudAuthServerApplication {

	public static void main(String[] args) {
		PearlSpringApplication.run("认证服务",PearlCloudAuthServerApplication.class, args);
	}

}
